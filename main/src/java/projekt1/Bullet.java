package projekt1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Bullet extends GameObject {

	public final static int SMALL = 3;
	public final static int MEDIUM = 6;

	private int size;
	private int speed;
	private int direction;
	private int penetration;

	public Bullet(int size, int speed, int direction, int penetration) {
		this.size = size;
		this.speed = speed;
		this.direction = direction;
		this.penetration = penetration;
	}

	/**
	 Kuuli joonistamine 
	  **/
	@Override
	public void draw(Graphics displayGraphics) {

		displayGraphics.setColor(Color.WHITE);
		displayGraphics.fillRect(getX(), getY(), size, size);
	}

	/**
	 Uuendab kuuli asukohta liites eelmine X positsiooni ja andes talle kiiruse, kuulide nurga
	  **/
	
	public void update() {

		int newX = getX() + speed;
		setX(newX);

		int newY = (int) (getY() + Math.sin(Math.toRadians((double) direction)) * speed);
		setY(newY);
	}

	/**
	  *Kuuli suurus
	  **/
	public Rectangle getRect() {
		return new Rectangle(getX(), getY(), size, size);
	}

	/**
	  *Kuuli damage
	  **/
	public int getDamage() {
		return 30;
	}

	/**
	  *Kuuli tabamine, mitu NPC l�bib kuni v��rtus 0 j�uab m��ratud weapon classis
	  **/
	public boolean hit() {

		penetration--;
		return penetration <= 0;
	}
}
