package projekt1;

import java.awt.Graphics;

public abstract class Drawable {
	
	public abstract void draw(Graphics displayGraphics);

}
