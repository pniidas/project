package projekt1;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Game extends JPanel implements ComponentListener, ActionListener {

	private Timer timer;
	private Timer spawnTimer;
	private GameScene scene;
	private PlayerController playerController;

	// annab game instancei game kontrollerile kaasa see = new...
	private GameController gameController = new GameController(this);

	public Game() {
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		initGame((int) screenSize.getWidth(), (int) screenSize.getHeight());

		JFrame frame = new JFrame();

		frame.setSize((int) screenSize.getWidth(), (int) screenSize.getHeight());
		frame.add(this);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.addComponentListener(this);

		setFocusable(true);
		requestFocusInWindow();

	}

	/**
	  *M�ngu startimine
	  **/
	public void initGame(int width, int height) {

		GameScene gamescene = new GameScene();

		Map map = new Map();
		map.setSize(width, height);

		Player player = new Player(gamescene);
		player.setX(100);
		player.setY(100);

		gamescene.setPlayer(player);

		gamescene.setMap(map);
		scene = gamescene;

		playerController = new PlayerController(player);
		playerController.setMinHeight(0);
		playerController.setMaxHeight(height);

		addKeyListener(playerController);

		timer = new Timer(50, this); // game klass implements Action
										// listener(see on mis this teeb)
		spawnTimer = new Timer(1500, this);

		timer.start();
		spawnTimer.start();

	}

	/**
	  *M�ngu loomine
	  **/
	public static void main(String[] a) {
		Game game = new Game();
	}

	/**
	  *Joonistab m�ngu �htlase intervalli tagant.
	  **/
	public void paint(Graphics displayGraphics) {

		scene.draw(displayGraphics);

		GameScene gamescene = (GameScene) scene;

		if (gamescene.isGameOver()) {
			timer.stop();
			spawnTimer.stop();

			// v�ttan player controlleri keylisteneri �ra
			removeKeyListener(playerController);
			// anna game controllerile kuulamise �le, et reseti t�ita
			addKeyListener(gameController);

			Font font = new Font("Serif", Font.PLAIN, 40);
			displayGraphics.setFont(font);
			displayGraphics.setColor(Color.RED);
			displayGraphics.drawString("GAME OVER! Press R to reset game", 400, 400);
		}
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {

	}

	@Override
	public void componentMoved(ComponentEvent arg0) {

	}

	/**
	 * Scene resize
	 **/
	@Override
	public void componentResized(ComponentEvent eventResize) {
		Component c = (Component) eventResize.getComponent();

		// scene != null sellep�rast, et kui ta on null siis pole olemas teda
		// aga kui on siis scene.rezise
		if (scene != null)
			scene.resize(c.getWidth(), c.getHeight()); //
		this.repaint();

	}

	@Override
	public void componentShown(ComponentEvent arg0) {

	}

	/**
	 * Timer callback
	 **/
	@Override
	public void actionPerformed(ActionEvent eventPerformed) {

		if (eventPerformed.getSource().equals(timer)) {

			playerController.updatePosition();
			scene.update();
			this.repaint();

		} else if (eventPerformed.getSource().equals(spawnTimer)) {

			GameScene gamescene = (GameScene) scene;

			if (gamescene.getNpcCount() > 50) {
				return;
			}

			int kills = gamescene.getKillCount();
			int spawnPerUpdate = (kills / 5) + 1;

			for (int i = 0; i < spawnPerUpdate; ++i) {

				NPC npc = new NPC((GameScene) scene);
				npc.setX(gamescene.getMap().getWidth() - 100);

				Random rand = new Random();

				// - npc.getHeight() et saada kaardi siseseselt hoida npc oli -1 ennem
				int randomY = rand.nextInt(gamescene.getMap().getHeight() - npc.getHeight());

				npc.setY(randomY);

				gamescene.addNpc(npc);
			}
		}
	}

	/**
	 * Game reset
	 **/
	public void reset() {

		// annan player controlleri keylisteneri
		addKeyListener(playerController);
		// v�ttan �ra game controllerile kuulamise
		removeKeyListener(gameController);
		scene.gameReset();

		timer.start();
		spawnTimer.start();

	}
}
