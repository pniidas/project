package projekt1;

import java.awt.Rectangle;

public abstract class GameObject extends Drawable {

	// m��rab iga asja positsiooni Player, NPC jne
	private int x;
	private int y;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void update() {
	}

	public abstract Rectangle getRect();

}
