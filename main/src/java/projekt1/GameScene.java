package projekt1;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

public class GameScene extends Scene {

	private static final int MAXZOMBIESPASSED = 15;

	private Map map;
	private Player player;
	private ArrayList<Bullet> bullets = new ArrayList<>();
	private ArrayList<NPC> npcs = new ArrayList<>();

	private int kills;
	private int zombiesPassed = 0;

	/**
	 * Draw meetod
	 **/
	@Override
	public void draw(Graphics displayGraphics) {

		map.draw(displayGraphics);

		drawGameObjects(npcs, displayGraphics);
		drawGameObjects(bullets, displayGraphics);

		// joonistab m�ngija
		player.draw(displayGraphics);

		// Draw kill count
		Font font = new Font("Serif", Font.PLAIN, 18);

		displayGraphics.setFont(font);
		displayGraphics.setColor(Color.RED);
		displayGraphics.drawString("KILLS: " + kills, map.getHeight() - 400, 20);
		displayGraphics.drawString("Zobies Passed: " + zombiesPassed, map.getWidth() - 200, 20);

	}

	/**
	 * Game reset parameetrite clear
	 **/
	public void gameReset() {

		bullets.clear();
		npcs.clear();
		kills = 0;
		zombiesPassed = 0;

	}

	/**
	 * M��rab GameScene map'i
	 **/
	public void setMap(Map map) {
		this.map = map;
	}

	/**
	 * M��rab GameScene player'i
	 **/
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * Muudab GameScene'i suurust
	 **/
	@Override
	public void resize(int w, int h) {

		map.setSize(w, h);
	}

	/**
	 * Lisab GameScene'i NPC
	 **/
	public void addNpc(NPC npc) {

		npcs.add(npc);
	}

	/**
	 * Lisab GameScene'i kuuli
	 **/
	public void addBullet(Bullet bullet) {
		bullets.add(bullet);
	}

	/**
	 * Tagastab aktiivsete NPC'de arvu
	 **/
	public int getNpcCount() {
		return npcs.size();
	}

	/**
	 * Update'ib GameScene objekte
	 **/
	public void update() {

		updateGameObjects(bullets, false);
		updateGameObjects(npcs, true);
		detectCollision();
	}

	/**
	 * Uuendab GameScene objekte
	 **/
	public <T extends GameObject> void updateGameObjects(ArrayList<T> gameObjects, boolean zombies) {
		Iterator<T> iterator = gameObjects.iterator();
		while (iterator.hasNext()) {

			GameObject gameObject = iterator.next();
			gameObject.update();
			if (gameObject.getX() > map.getWidth() || gameObject.getY() > map.getHeight() || gameObject.getX() < 0) {

				if (zombies) {
					zombiesPassed++;
				}

				iterator.remove();
			}
		}
	}

	/**
	 * Joonistab GameScene'i objekte
	 **/
	public <T extends GameObject> void drawGameObjects(ArrayList<T> gameObjects, Graphics displayGraphics) {
		// joonistab k�ik m�ngu elemendid
		for (GameObject gameObject : gameObjects) {
			gameObject.draw(displayGraphics);
		}
	}

	/**
	 * Tagastab GameScene map'i
	 **/
	public Map getMap() {
		return map;
	}

	/**
	 * Tuvastab GameScene'i kokkup�rkeid
	 **/
	public void detectCollision() {

		Iterator<Bullet> bulletIterator = bullets.iterator();
		while (bulletIterator.hasNext()) {

			Bullet bullet = bulletIterator.next();
			Rectangle bulletRect = bullet.getRect();

			Iterator<NPC> npcIterator = npcs.iterator();
			while (npcIterator.hasNext()) {

				NPC npc = npcIterator.next();
				if (bulletRect.intersects(npc.getRect())) {

					if (npc.hit(bullet)) {
						kills++;
						npcIterator.remove();
					}

					if (bullet.hit()) {
						bulletIterator.remove();
						break;
					}
				}
			}
		}
	}

	/**
	 * Tagastab kill counti
	 **/
	public int getKillCount() {
		return kills;
	}

	/**
	 * Kontrollib kas m�ng on l�bi
	 **/
	public boolean isGameOver() {
		return zombiesPassed >= MAXZOMBIESPASSED;
	}

}
