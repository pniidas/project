package projekt1;

import java.awt.Color;
import java.awt.Graphics;

public class LifeBar {

	private static final int LIFEBAR_HEIGHT = 20;

	/** 
	 * Joonistab lifebar'i
	 * 
	 * @param x life bar x kordinaat
	 * @param y life bar y kordinaat
	 * @param w life bar laius
	 * @param p life bar alles olevate elude protsent
	 * @param displayGraphics life bar class mille abil life bar joonistada
	 **/
	public static void draw(int x, int y, int w, int p, Graphics displayGraphics) {

		displayGraphics.setColor(Color.GREEN);
		displayGraphics.fillRect(x, y - LIFEBAR_HEIGHT, w * p / 100, LIFEBAR_HEIGHT);

		displayGraphics.setColor(Color.RED);
		displayGraphics.fillRect(x + w * p / 100, y - LIFEBAR_HEIGHT, w * (100 - p) / 100, LIFEBAR_HEIGHT);
	}
}
