package projekt1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends Drawable {

	private int h;
	private int w;
	// private Sprite sprite = new Sprite("/ground.jpg", false); // background image

	/**
	 * Joonistab kaardi
	 **/
	public void draw(Graphics displayGraphics) {

		displayGraphics.setColor(Color.BLACK);
		displayGraphics.fillRect(0, 0, w, h);

		// sprite.draw(0, 0, displayGraphics); // draw sprite, background image

	}

	/**
	 * M��rab suuruse
	 **/
	public void setSize(int w, int h) {

		this.h = h;
		this.w = w;

	}

	/**
	 * Tagastab kaardi laiuse
	 **/
	public int getWidth() {

		return w;
	}

	/**
	 * Tagastab kaardi k�rguse
	 **/
	public int getHeight() {

		return h;
	}

}
