package projekt1;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class NPC extends GameObject {

	private static final String ZOMBIE_SPRITE = "/skeleton-idle_16.png";
	private int speed = 5;
	private int lives = 100;
	private static final Sprite sprite = new Sprite(ZOMBIE_SPRITE, true);
	private GameScene gameScene;

	/**
	 * NPC konstruktor
	 **/
	public NPC(GameScene gamescene) {
		this.gameScene = gamescene;
		Random rand = new Random();
		speed = rand.nextInt(10) + 5; // variable speed zombie
	}

	/**
	 * Joonistab NPC ja lifebar'i
	 **/
	@Override
	public void draw(Graphics displayGraphics) {
		sprite.draw(getX(), getY(), displayGraphics);
		LifeBar.draw(getX(), getY(), sprite.getWidth(), lives, displayGraphics);
	}

	/**
	 * Tagastab NPC liikumis kiiruse
	 **/
	public int getSpeed() {
		return speed;
	}

	/**
	 * NPC k�rgus
	 **/
	public int getHeight() {
		return sprite.getHeight();
	}
	/**
	 * M��rab GameScene player'i
	 **/
	public void update(){
		//zombie liikuma saada annad x kordinaadi ja lahutad kiiruse
		setX(getX() - speed);
		
	}
	/**
	 * Tagastab NPC ruudu 
	 **/
	public Rectangle getRect() {
		// TODO Auto-generated method stub
		return new Rectangle(getX(), getY(), getHeight(), getWidth());
	}

	/**
	 * Tagastab NPC laiuse
	 **/
	private int getWidth() {
		// TODO Auto-generated method stub
		
		
		return sprite.getWidth();
	}

	/**
	 * Teavitab NPC pihta lasust ning lahutab lasu tugevuse v��rtuse
	 * 
	 * @return tagastab true kui NPC'l on veel elusi j�rgi
	 **/
	public boolean hit(Bullet bullet) {
		
		lives -= bullet.getDamage();
		
		return lives <= 0;
	}

}
