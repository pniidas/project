package projekt1;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;

public class Player extends GameObject {

	private static final String HANDGUN_SPRITE = "/survivor-shoot_handgun_0.png";
	private static final String SHOTGUN_SPRITE = "/survivor-shoot_shotgun_0.png";
	private static final String RIFLE_SPRITE = "/survivor-shoot_rifle_0.png";
	
	private int speed = 10;
	private Sprite playerSprite = new Sprite(SHOTGUN_SPRITE, false);
	private GameScene gameScene;
	
	private Weapon weapon;
	
	
	/**
	 * Player konstruktor
	 **/
	public Player(GameScene gamescene){
		
		this.gameScene = gamescene;
		this.weapon = new ShotGun();
	}
	
	/**
	 * Joonistab m�ngija
	 **/
	@Override
	public void draw(Graphics displayGraphics) {
		
		playerSprite.draw(getX(), getY(), displayGraphics);
		
		// Draw weapon stats
		Font font = new Font("Serif", Font.PLAIN, 18);
		displayGraphics.setFont(font);
		
		displayGraphics.setColor(Color.RED);
		displayGraphics.drawString("WEAPON: " + weapon.getName(), 10, 20);
	}

	/**
	 * tagastab m�ngija kiiruse
	 **/
	public int getSpeed(){
		return speed;
	}

	/**
	 * Tagastab m�ngija k�rguse
	 **/
	public int getHeight() {
		return playerSprite.getHeight();
	}

	/**
	 * M�ngija tulistab 
	 **/
	public void shoot() {

		ArrayList<Bullet> bullets = weapon.shoot(getX() + playerSprite.getWidth(), getY() + playerSprite.getHeight() / 2);
		
		for (Bullet bullet : bullets)
			gameScene.addBullet(bullet);
	}

	/**
	 * Pole kasutusel
	 **/
	@Override
	public Rectangle getRect() {
		return null;
	}
	

	
}
