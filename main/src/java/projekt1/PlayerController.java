package projekt1;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PlayerController implements KeyListener {

	// static muutujad on alati koodi alguses m��ratud siis mitte staatilised,
	// m��rad k�ik v�imalikud liikumised, teen bitwise operate'oritega siis ei
	// pea olema m��ratud if'dega
	private static final int DIRECTION_UP = 1;
	private static final int DIRECTION_DOWN = 2;
	private static final int DIRECTION_RIGHT = 4;
	private static final int DIRECTION_LEFT = 8;

	private int movementDirection = 0; // set default direction value to 0
	private Player player;
	private int maxHeight;
	private int minHeight;

	/**
	 * PlayerController konstruktor
	 **/
	public PlayerController(Player player) {

		this.player = player;

	}

	/**
	 * Nuppuvajutuse callback
	 **/
	@Override
	public void keyPressed(KeyEvent kP) {

		

		int key = kP.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			setMovementDirection(DIRECTION_LEFT);
		}

		if (key == KeyEvent.VK_RIGHT) {
			setMovementDirection(DIRECTION_RIGHT);
		}

		if (key == KeyEvent.VK_UP) {
			setMovementDirection(DIRECTION_UP);
		}

		if (key == KeyEvent.VK_DOWN) {
			setMovementDirection(DIRECTION_DOWN);
		}

		
		if (key == KeyEvent.VK_SPACE) {

			player.shoot();

	
		}

	}

	/**
	 * Nuppu lahtilaskmise callback
	 **/
	@Override
	public void keyReleased(KeyEvent kR) {

		int key = kR.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			removeDirection(DIRECTION_LEFT);
		}

		if (key == KeyEvent.VK_RIGHT) {
			removeDirection(DIRECTION_RIGHT);
		}

		if (key == KeyEvent.VK_UP) {
			removeDirection(DIRECTION_UP);
		}

		if (key == KeyEvent.VK_DOWN) {
			removeDirection(DIRECTION_DOWN);
		}

	}

	/**
	 * Uuendab asukohta vastaalt sellele mis nuppud on alla vajutatud
	 **/
	public void updatePosition() {

		if ((movementDirection & DIRECTION_UP) == DIRECTION_UP) {

			calculateNewPosition(0, -1);

		} else if ((movementDirection & DIRECTION_DOWN) == DIRECTION_DOWN) {

			calculateNewPosition(0, 1);

		} else if ((movementDirection & DIRECTION_RIGHT) == DIRECTION_RIGHT) {

			calculateNewPosition(1, 0);

		} else if ((movementDirection & DIRECTION_LEFT) == DIRECTION_LEFT) {

			calculateNewPosition(-1, 0);

		}

	}

	/**
	 * Arvutab uue m�ngija asukoha
	 **/
	public void calculateNewPosition(int stepX, int stepY) {

		// k�sib player X kordinaadi ja lisab sellele stepX'i siis paneb
		// liidetud kordinaadi summa player X kordinaadi v��rtuseks
		// player.setX(player.getX() + stepX * player.getSpeed());

		// player object, ja samale objectile liidan muutuja(on muutuja ja sama
		// aegselt funktsiooni argument)
		int newY = player.getY() + stepY * player.getSpeed();
	
		if (newY > minHeight && newY + player.getHeight() < maxHeight) {

			player.setY(newY);
		}

	}

	/**
	 * Tagastab liikumis suuna
	 **/
	public int getMovementDirection() {

		return movementDirection;
	}

	/**
	 * M��ran liikumis suuna
	 **/
	public void setMovementDirection(int movementDirection) {
		this.movementDirection |= movementDirection;
	}

	/**
	 * Eemaldab nuppu vajutuse
	 **/
	public void removeDirection(int direction) {
		// v�ttab ainult �he movement'i maha olenevat mis key lahti lasti
		this.movementDirection ^= direction; //
	}

	/**
	 * M��ran minimaalse k�rguse
	 **/
	public void setMinHeight(int height) {
		this.minHeight = height;

	}
	/**
	 * M��ran maksimaalse k�rguse
	 **/
	public void setMaxHeight(int height) {
		this.maxHeight = height;
	}

	/**
	 * Kasutamatta
	 **/
	@Override
	public void keyTyped(KeyEvent arg0) {

	}

}
