package projekt1;

import java.awt.Graphics;

//kui teha Scene class'le extends siis on vaja �ra implementida draw meetod
public abstract class Scene {
	

	// draw meetod mis joonistab scene'i
	public abstract void draw(Graphics displayGraphics);
	
	public abstract void resize(int w, int h);

	public abstract void update(); 

		
		
		
	

}
