package projekt1;

import java.util.ArrayList;

public class ShotGun extends Weapon {
	
	private static final String NAME = "SHOTGUN";
	
	private int bulletsPerShot = 6;
	private int shootingAngle = 10;
	
	// -15 -10 -5 0 5 10 15
	
	/**
	 * Shotgun konstruktor
	 **/
	public ShotGun() {
		super(NAME);
		
	}

	/**
	 * Tulistab ja loob kuulid
	 **/
	@Override
	public ArrayList<Bullet> shoot(int x, int y) {
	
		ArrayList<Bullet> bullets = new ArrayList<>();
		
		int halfNegAngle = shootingAngle/2 - shootingAngle;
		int angleStep = shootingAngle/bulletsPerShot;
		
		for (int i = 0; i < bulletsPerShot; i++) {
			//bullet size medium, kiirus 100, l'heb otse ja l'bib 3me
			Bullet bullet = new Bullet(Bullet.SMALL, 70, halfNegAngle + angleStep * i, 10); //10 on penetration value

			bullet.setX(x);
			bullet.setY(y);
			
			bullets.add(bullet);
		}
		
		return bullets;
	}

}
