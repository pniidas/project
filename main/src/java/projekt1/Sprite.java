package projekt1;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Sprite {

	private int currentFrame = 0;
	private boolean flipImage = false;

	private ArrayList<Image> frames = new ArrayList<>();

	/**
	 * Sprite konstruktor
	 **/
	public Sprite(String file, boolean flip) {

		URL url = this.getClass().getResource(file); // "/survivor-shoot_handgun_0.png"

		try {
			Image image = ImageIO.read(url);
			image = (flip)?flipHorizontal((BufferedImage)image): image;
			frames.add(image);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Joonistan Sprite'i
	 **/
	public void draw(int x, int y, Graphics graphics) {

		Graphics2D graphics2d = (Graphics2D) graphics;
		graphics2d.drawImage(frames.get(currentFrame), x, y, null);

	}

	/**
	 * M��ran Sprite k�rguse
	 **/
	public int getHeight() {

		return frames.get(currentFrame).getHeight(null); // peab null olema sest
															// sa ei taha
															// kuulata kui
															// k�rgus muutub
	}

	/**
	 * Keerab pilti horizontaalselt
	 **/
	public BufferedImage flipHorizontal(BufferedImage bufferedImage) {

		AffineTransform tx = AffineTransform.getScaleInstance(-1, -1);
		tx.translate(-bufferedImage.getWidth(null), -bufferedImage.getHeight(null));
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		return op.filter(bufferedImage, null);

	}

	/**
	 * Tagastan Sprite'i laiust
	 **/
	public int getWidth() {
		return frames.get(currentFrame).getWidth(null);
	}

}
