package projekt1;

import java.util.ArrayList;

public abstract class Weapon {

	private String name;

	/**
	 * Relva konstruktor
	 **/
	public Weapon(String name) {

		this.name = name;
	}

	
	public abstract ArrayList<Bullet> shoot(int x, int y);

	/**
	 * Relva nime tagastamine
	 **/
	public String getName() {
		return name;
	}
}
